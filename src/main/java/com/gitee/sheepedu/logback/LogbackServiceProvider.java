package com.gitee.sheepedu.logback;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.classic.util.ContextSelectorStaticBinder;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.status.StatusUtil;
import ch.qos.logback.core.util.StatusPrinter;
import org.slf4j.ILoggerFactory;
import org.slf4j.helpers.Util;
import org.slf4j.spi.LoggerFactoryBinder;

/**
 * LogbackServiceProvider
 * @author: sunj
 * @since 1.0.0
 *
 **/
public class LogbackServiceProvider implements LoggerFactoryBinder {

    private static Object KEY = new Object();
    private static Object lock = new Object();
    private boolean initialized = false;
    private static LoggerContext defaultLoggerContext = new LoggerContext();
    private final ContextSelectorStaticBinder contextSelectorBinder = ContextSelectorStaticBinder.getSingleton();

    private volatile static LogbackServiceProvider logbackServiceProvider;

    public static LogbackServiceProvider getInstance(){
        if(logbackServiceProvider == null){
            synchronized (lock){
                if(logbackServiceProvider == null){
                    return new LogbackServiceProvider();
                }
            }
        }
        return logbackServiceProvider;
    }

    private LogbackServiceProvider(){
        init();
    }

    void init() {
        try {
            try {
                (new ContextInitializer(defaultLoggerContext)).autoConfig();
            } catch (JoranException var2) {
                Util.report("Failed to auto configure default logger context", var2);
            }

            if (!StatusUtil.contextHasStatusListener(this.defaultLoggerContext)) {
                StatusPrinter.printInCaseOfErrorsOrWarnings(this.defaultLoggerContext);
            }

            this.contextSelectorBinder.init(this.defaultLoggerContext, KEY);
            this.initialized = true;
        } catch (Exception var3) {
            Util.report("Failed to instantiate [" + LoggerContext.class.getName() + "]", var3);
        }

    }

    public ILoggerFactory getLoggerFactory() {
        if (!this.initialized) {
            return this.defaultLoggerContext;
        } else if (this.contextSelectorBinder.getContextSelector() == null) {
            throw new IllegalStateException("contextSelector cannot be null. See also http://logback.qos.ch/codes.html#null_CS");
        } else {
            return this.contextSelectorBinder.getContextSelector().getLoggerContext();
        }
    }

    public String getLoggerFactoryClassStr() {
        return this.contextSelectorBinder.getClass().getName();
    }

    public static void setColorName(String colorName) {
        defaultLoggerContext.putProperty("colorName",colorName);
    }

    public static void setLoggerName(String loggerName) {
        defaultLoggerContext.putProperty("loggerName",loggerName);
    }
}
