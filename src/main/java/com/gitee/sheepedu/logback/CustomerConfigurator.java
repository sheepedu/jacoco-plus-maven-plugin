package com.gitee.sheepedu.logback;


import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.Configurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.status.Status;

import java.nio.charset.Charset;

/**
 * CustomerConfigurator
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class CustomerConfigurator implements Configurator{

    @Override
    public void configure(LoggerContext lc) {

        String color = lc.getCopyOfPropertyMap().get("colorName");
        String loggerName = lc.getCopyOfPropertyMap().get("loggerName");

        ConsoleAppender<ILoggingEvent> ca = new ConsoleAppender<ILoggingEvent>();
//        ca.setWithJansi(true);
        ca.setContext(lc);
        ca.setName("console");

        PatternLayoutEncoder layout = new PatternLayoutEncoder();
        layout.setCharset(Charset.forName("UTF-8"));
        layout.setOutputPatternAsHeader(true);
        String tag = LoggerColorEnums.getTagByName(color);
        if(tag == null){
            layout.setPattern("[%thread] %-5level %msg %n");
        }else {
            layout.setPattern("[%thread] %-5level "+tag+"(%msg %n)");
        }

        layout.setContext(lc);
        layout.start();

        ca.setEncoder(layout);
        ca.start();

        if(loggerName == null){
            loggerName = Logger.ROOT_LOGGER_NAME;
        }
        Logger rootLogger = lc.getLogger(loggerName);
        rootLogger.addAppender(ca);

    }

    @Override
    public void setContext(Context context) {

    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void addStatus(Status status) {

    }

    @Override
    public void addInfo(String msg) {

    }

    @Override
    public void addInfo(String msg, Throwable ex) {

    }

    @Override
    public void addWarn(String msg) {

    }

    @Override
    public void addWarn(String msg, Throwable ex) {

    }

    @Override
    public void addError(String msg) {

    }

    @Override
    public void addError(String msg, Throwable ex) {

    }

}
