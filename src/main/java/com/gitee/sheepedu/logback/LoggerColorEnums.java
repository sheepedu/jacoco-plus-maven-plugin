package com.gitee.sheepedu.logback;

import java.util.Arrays;
import java.util.Optional;

public enum LoggerColorEnums {

    black("black","%black"),
    red("red","%red"),
    green("green","%green"),
    yellow("yellow","%yellow"),
    blue("blue","%blue"),
    magenta("magenta","%magenta"),
    cyan("cyan","%cyan"),
    white("white","%white"),
    gray("gray","%gray"),
    boldRed("boldRed","%boldRed"),
    boldGreen("boldGreen","%boldGreen"),
    boldYellow("boldYellow","%boldYellow"),
    boldBlue("boldBlue","%boldBlue"),
    boldMagenta("boldMagenta","%boldMagenta"),
    boldCyan("boldCyan","%boldCyan"),
    boldWhite("boldWhite","%boldWhite"),
    highlight("highlight","%highlight"),

    ;


    private String name;

    private String tag;

    LoggerColorEnums(String name, String tag) {
        this.name = name;
        this.tag = tag;
    }

    public static String getTagByName(String colorName){
        if(colorName == null || colorName.length() == 0){
            return null;
        }

        Optional<LoggerColorEnums> first = Arrays.stream(values()).filter((c -> c.name.equals(colorName))).findFirst();

        if (first.isPresent()){
            return first.get().tag;
        }
        return null;

    }
}
