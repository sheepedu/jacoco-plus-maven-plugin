package com.gitee.sheepedu;

import com.gitee.sheepedu.logback.LogbackServiceProvider;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.data.ExecutionData;
import org.jacoco.core.data.SessionInfo;
import org.jacoco.report.IReportGroupVisitor;
import org.jacoco.report.IReportVisitor;
import org.jacoco.report.ISourceFileLocator;
import org.slf4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * ConsoleLoggerReportVisitor
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class ConsoleLoggerReportVisitor implements IReportVisitor {

    private Logger logger;

    private final AbstractReportMojo mojo;

    private final Locale locale;


    public ConsoleLoggerReportVisitor(AbstractReportMojo mojo, Locale locale, String color) {
        this.mojo = mojo;
        this.locale = locale;
        LogbackServiceProvider.setColorName(color);
        LogbackServiceProvider.setLoggerName(ConsoleLoggerReportVisitor.class.getName());
        LogbackServiceProvider logbackServiceProvider = LogbackServiceProvider.getInstance();
        logger = logbackServiceProvider.getLoggerFactory().getLogger(ConsoleLoggerReportVisitor.class.getName());
    }


    public void visitInfo(List<SessionInfo> sessionInfos, Collection<ExecutionData> executionData) throws IOException {

    }

    public void visitBundle(IBundleCoverage bundle, ISourceFileLocator locator) throws IOException {
        logger.info("name:"+bundle.getName());

        ICounter classCounter = bundle.getClassCounter();
        int totalCount = classCounter.getTotalCount();
        if(totalCount == 0){
            logger.warn("Class totalCount equals 0");
        }else {
            BigDecimal classCoverage = new BigDecimal(classCounter.getCoveredCount())
                    .divide(new BigDecimal(totalCount),2, BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));

            logger.info("ClassCoverage:"+ classCoverage.toString()+"%");
        }


        ICounter methodCounter = bundle.getMethodCounter();
        int methodCounterTotalCount = methodCounter.getTotalCount();

        if(methodCounterTotalCount == 0){
            logger.warn("Method totalCount equals 0");
        }else{
            BigDecimal methodCoverage = new BigDecimal(methodCounter.getCoveredCount())
                    .divide(new BigDecimal(methodCounterTotalCount),2,BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));;
            logger.info("MethodCoverage:"+ methodCoverage.toString()+"%");
        }

        ICounter lineCounter = bundle.getLineCounter();
        int lineCounterTotalCount = lineCounter.getTotalCount();
        if(lineCounterTotalCount == 0){
            logger.warn("Line totalCount equals 0");
        }else {
            BigDecimal lineCoverage = new BigDecimal(lineCounter.getCoveredCount())
                    .divide(new BigDecimal(lineCounterTotalCount),2, BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));;
            logger.info("LineCoverage:"+ lineCoverage.toString()+"%");
        }

        ICounter complexityCounter = bundle.getComplexityCounter();
        int complexityCounterTotalCount = complexityCounter.getTotalCount();
        if(complexityCounterTotalCount == 0){
            logger.warn("Complexity totalCount equals 0");
        }else {
            BigDecimal complexityCoverage = new BigDecimal(complexityCounter.getCoveredCount())
                    .divide(new BigDecimal(complexityCounterTotalCount),2, BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));;
            logger.info("ComplexityCoverage:"+ complexityCoverage.toString()+"%");
        }

        ICounter branchCounter = bundle.getBranchCounter();
        int branchCounterTotalCount = branchCounter.getTotalCount();
        if(branchCounterTotalCount == 0){
            logger.warn("Branch totalCount equals 0");
        }else {
            BigDecimal branchCoverage = new BigDecimal(branchCounter.getCoveredCount())
                    .divide(new BigDecimal(branchCounterTotalCount),2, BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));;
            logger.info("BranchCoverage:"+ branchCoverage.toString()+"%");
        }

        ICounter instructionCounter = bundle.getInstructionCounter();
        int instructionCounterTotalCount = instructionCounter.getTotalCount();
        if(instructionCounterTotalCount == 0){
            logger.warn("Instruction totalCount equals 0");
        }else {
            BigDecimal instructionCoverage = new BigDecimal(instructionCounter.getCoveredCount())
                    .divide(new BigDecimal(instructionCounterTotalCount),2, BigDecimal.ROUND_HALF_DOWN)
                    .multiply(new BigDecimal(100));;
            logger.info("InstructionCoverage:"+ instructionCoverage.toString()+"%");
        }
    }

    public IReportGroupVisitor visitGroup(String name) throws IOException {
        return new IReportGroupVisitor() {
            @Override
            public void visitBundle(IBundleCoverage bundle, ISourceFileLocator locator) throws IOException {
            }

            @Override
            public IReportGroupVisitor visitGroup(String name) throws IOException {
                return this;
            }
        };
    }


    public void visitEnd() throws IOException { }

}
