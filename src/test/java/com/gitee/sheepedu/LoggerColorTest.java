package com.gitee.sheepedu;

import com.gitee.sheepedu.logback.LogbackServiceProvider;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * LoggerColorTest
 *
 * @author: sunj
 * @since 1.0.0
 **/
public class LoggerColorTest {

    private Logger logger;

    @Test
    public void test(){
        LogbackServiceProvider.setColorName("yellow");
        LogbackServiceProvider.setLoggerName(ConsoleLoggerReportVisitor.class.getName());
        LogbackServiceProvider instance = LogbackServiceProvider.getInstance();
        logger = instance.getLoggerFactory().getLogger(ConsoleLoggerReportVisitor.class.getName());
        logger.info("logger-color");
    }

}
